#include <cmath>
#include <fstream>
#include <iomanip>
#include <limits>
#include <stdexcept>
#include <string>
#include <vector>
#include <omp.h>
#include <chrono>

#define MAX3(x, y, z) \
    ( ((x)>(y) && (x)>(z)) ? (x) : ((y)>(z)) ? (y) : (z) )
#define MAX2(x, y) ( ((x) < (y)) ? (y) : (x))
#define MIN2(x, y) ( ((x) < (y)) ? (x) : (y))

#define NUM_GPU 2

// assert MAX_PARTICLES is even (for find minmax)
#define MAX_PARTICLES 1024
#define MAX_DEVICES 4
#define DT (60.0)
#define THETA (0.25)
#define EPS (1e-3)
#define CONSTANT_G 1e-5*6.674e-6
#define MISSLE_SPEED (1e6)
#define N_STEPS 200000
#define PLANET_RADIUS (1e7)

#define RECURSION_STACK_SIZE 12 

// NUMWORKER: 8, 16, 32, 64; NUMWORKER_POW: 3, 4, 5, 6
#define NUMBLOCKS 128
#define THREADS_PER_BLOCK 320 // THREADS_PER_BLOCK should be multiple of NUMWORKER
#define NUMWORKER 64
#define NUMWORKER_POW 6

uint64_t getTimestamp(){
    using namespace std::chrono;
    return duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
}

namespace param {
    const int n_steps = 200000;
    const double dt = 60;
    const double eps = 1e-3;
    const double G = 6.674e-11;
    double gravity_device_mass(double m0, double t) {
        return m0 + 0.5 * m0 * fabs(sin(t / 6000));
    }
    const double planet_radius = 1e7;
    const double missile_speed = 1e6;
    double get_missile_cost(double t) { return 1e5 + 1e3 * t; }

    const double theta = 0.0;
}  // namespace param

typedef struct _PendingCaseArg {
    unsigned int deviceID;
    int collsionStep;
} PendingCaseArg;

int N_h;
int N_padding_h;
int planetId_h; 
int asteroidId_h;

int *N_tmp[2], *planetId_tmp[2], *asteroidId_tmp[2];
double *Input_qxV_tmp[2], *Input_qyV_tmp[2], *Input_qzV_tmp[2];
double *Input_vxV_tmp[2], *Input_vyV_tmp[2], *Input_vzV_tmp[2];
double *Input_mV_tmp[2];
char *Input_typeV_tmp[2];

int* N_padding_tmp[2];
double *bhTree_bxminV_tmp[2], *bhTree_byminV_tmp[2], *bhTree_bzminV_tmp[2];
double *bhTree_bxmaxV_tmp[2], *bhTree_bymaxV_tmp[2], *bhTree_bzmaxV_tmp[2];
double *bhTree_cxV_tmp[2], *bhTree_cyV_tmp[2], *bhTree_czV_tmp[2];
double *bhTree_mV_tmp[2], *bhTree_widthV_tmp[2];

int* numPendingCase_tmp;
double* Pending_qxVV_tmp[MAX_DEVICES], *Pending_qyVV_tmp[MAX_DEVICES], *Pending_qzVV_tmp[MAX_DEVICES];
double* Pending_vxVV_tmp[MAX_DEVICES], *Pending_vyVV_tmp[MAX_DEVICES], *Pending_vzVV_tmp[MAX_DEVICES];
double* Pending_mVV_tmp[MAX_DEVICES];
char* Pending_typeVV_tmp[MAX_DEVICES];
PendingCaseArg* pendingCaseArgV_tmp;

__constant__ int* N_cnst;
__constant__ int* planetId_cnst;
__constant__ int* asteroidId_cnst;
__constant__ double* Input_qxV_cnst;
__constant__ double* Input_qyV_cnst;
__constant__ double* Input_qzV_cnst;
__constant__ double* Input_vxV_cnst;
__constant__ double* Input_vyV_cnst;
__constant__ double* Input_vzV_cnst;
__constant__ double* Input_mV_cnst;
__constant__ char* Input_typeV_cnst;

__constant__ int* N_padding_cnst;
__constant__ double* bhTree_bxminV_cnst;
__constant__ double* bhTree_byminV_cnst;
__constant__ double* bhTree_bzminV_cnst;
__constant__ double* bhTree_bxmaxV_cnst;
__constant__ double* bhTree_bymaxV_cnst;
__constant__ double* bhTree_bzmaxV_cnst;
__constant__ double* bhTree_cxV_cnst;
__constant__ double* bhTree_cyV_cnst;
__constant__ double* bhTree_czV_cnst;
__constant__ double* bhTree_mV_cnst;
__constant__ double* bhTree_widthV_cnst;


int numPendingCase_h = 0;
// double* Pending_qxVV_h[MAX_DEVICES], *Pending_qyVV_h[MAX_DEVICES], *Pending_qzVV_h[MAX_DEVICES];
// double* Pending_vxVV_h[MAX_DEVICES], *Pending_vyVV_h[MAX_DEVICES], *Pending_vzVV_h[MAX_DEVICES];
// double* Pending_mVV_h[MAX_DEVICES];
// char* Pending_typeVV_h[MAX_DEVICES];
PendingCaseArg pendingCaseArgV_h[MAX_DEVICES];


__constant__ int* numPendingCase_cnst;
__constant__ double* Pending_qxVV_cnst[MAX_DEVICES], *Pending_qyVV_cnst[MAX_DEVICES], *Pending_qzVV_cnst[MAX_DEVICES];
__constant__ double* Pending_vxVV_cnst[MAX_DEVICES], *Pending_vyVV_cnst[MAX_DEVICES], *Pending_vzVV_cnst[MAX_DEVICES];
__constant__ double* Pending_mVV_cnst[MAX_DEVICES];
__constant__ char* Pending_typeVV_cnst[MAX_DEVICES];
__constant__ PendingCaseArg* pendingCaseArgV_cnst;


void read_input(const char* filename, int& N_h, int& planetId_h, int& asteroidId_h,
    std::vector<double>& qxV_h, std::vector<double>& qyV_h, std::vector<double>& qzV_h,
    std::vector<double>& vxV_h, std::vector<double>& vyV_h, std::vector<double>& vzV_h,
    std::vector<double>& mV_h, std::vector<char>& typeV_h) {

    std::ifstream fin(filename);
    fin >> N_h >> planetId_h >> asteroidId_h;
    qxV_h.resize(N_h);
    qyV_h.resize(N_h);
    qzV_h.resize(N_h);
    vxV_h.resize(N_h);
    vyV_h.resize(N_h);
    vzV_h.resize(N_h);
    mV_h.resize(N_h);
    typeV_h.resize(N_h);
    for (int i = 0; i < N_h; i++) {
        std::string str;
        fin >> qxV_h[i] >> qyV_h[i] >> qzV_h[i] >> vxV_h[i] >> vyV_h[i] >> vzV_h[i] >> mV_h[i] >> str;
        if (str == "device") typeV_h[i] = 'd';
        else typeV_h[i] = 'o';
    }
}

void write_output(const char* filename, double min_dist, int hit_time_step,
    int gravity_device_id, double missile_cost) {

    std::ofstream fout(filename);
    fout << std::scientific
         << std::setprecision(std::numeric_limits<double>::digits10 + 1) << min_dist
         << '\n'
         << hit_time_step << '\n'
         << gravity_device_id << ' ' << missile_cost << '\n';
}

// return smallest N* s.t. N* >= N and N* is the power of 2
int get_N_padding(int N){
    int msnz_bit = __builtin_clz(N);
    int retVal = 1 << (sizeof(int)*8 - msnz_bit - 1);
    if ((N^retVal) != 0) retVal = retVal << 1; // need padding
    return retVal;
}

inline cudaError_t checkCuda(cudaError_t result) {
  if (result != cudaSuccess) 
    fprintf(stderr, "CUDA Runtime Error: %s\n", cudaGetErrorString(result));
  return result;
}

// Expands a 10-bit integer into 30 bits by inserting 2 zeros after each bit
__device__ unsigned int expandBits(unsigned int v) {
    v = (v * 0x00010001u) & 0xFF0000FFu;
    v = (v * 0x00000101u) & 0x0F00F00Fu;
    v = (v * 0x00000011u) & 0xC30C30C3u;
    v = (v * 0x00000005u) & 0x49249249u;
    return v;
}
// Calculates a 30-bit Morton code for the given 3D point located (first scale into the unit cube [0,1])
__device__ unsigned int morton3D(double x, double y, double z, double xmin, double xmax, double ymin, double ymax, double zmin, double zmax) {
    // printf("id[%d]original scaled x=%e y=%e z=%e\n", threadIdx.x, (x-xmin)/(xmax-xmin), (y-ymin)/(ymax-ymin), (z-zmin)/(zmax-zmin));
    x = MIN2(MAX2((x-xmin)/(xmax-xmin) * 1024.0f, 0.0f), 1023.0f);
    y = MIN2(MAX2((y-ymin)/(ymax-ymin) * 1024.0f, 0.0f), 1023.0f);
    z = MIN2(MAX2((z-zmin)/(zmax-zmin) * 1024.0f, 0.0f), 1023.0f);
    // printf("id[%d] x=%lf y=%lf z=%lf\n", threadIdx.x, x, y, z);
    unsigned int xx = expandBits((unsigned int)x);
    unsigned int yy = expandBits((unsigned int)y);
    unsigned int zz = expandBits((unsigned int)z);
    return (xx << 2) | (yy << 1) | zz;
}

__device__ void bhTree_find_minAndmax(double* Input_qA_dev, double& minOut, double& maxOut, double* buf_min, double* buf_max){
    // assert <<<1, N>>>

    buf_max[threadIdx.x] = buf_min[threadIdx.x] = Input_qA_dev[threadIdx.x];
    __syncthreads();

    for (int i=((blockDim.x>>1)+(blockDim.x&1)); i >= 2; i=(i>>1)+(i&1)){ // ceil(i/2)
        if (threadIdx.x < i){
            buf_min[threadIdx.x] = MIN2(buf_min[threadIdx.x], buf_min[threadIdx.x + i]);
            buf_max[threadIdx.x] = MAX2(buf_max[threadIdx.x], buf_max[threadIdx.x + i]);
        }
        __syncthreads();
    }

    if (threadIdx.x == 0) {
        minOut = MIN2(buf_min[0], buf_min[1]);
        maxOut = MAX2(buf_max[0], buf_max[1]);
        // printf("%lf, %lf\n", minOut, maxOut);
    }
}

__device__ void swap(unsigned int* &a, unsigned int* &b){
    unsigned int *temp = a;
    a = b;
    b = temp;
}

// Cnt: count # of elements belongs to 0~7; Offset: count relative offset
__device__ void radix_sort_Morton(unsigned int* Morton, unsigned int* Permutation, int* Cnt, int16_t* Offset){
    // assert <<<1, N>>> , N >= 8
    // sort every 3 digits, total 30 digits 

    unsigned int* Morton_In = &Morton[0];
    unsigned int* Morton_Out = &Morton[blockDim.x]; // blockDim.x == N
    unsigned int* Permutation_In = &Permutation[0];
    unsigned int* Permutation_Out = &Permutation[blockDim.x]; // blockDim.x == N

    #pragma unroll
    for (int i = 0; i < 10; i++){

        // init Counter to zeros
        __syncthreads();
        if (threadIdx.x < 8) Cnt[threadIdx.x] = 0;
        Offset[threadIdx.x] = 0;
        __syncthreads();

        // cal counter
        atomicAdd(&Cnt[ (Morton_In[threadIdx.x] >> (i*3)) & 7 ], 1); // 7 = 00000111
        __syncthreads();

        // cal exclusive prefix sum
        if (threadIdx.x == 0){
            int lastVal = Cnt[0];
            Cnt[0] = 0;
            for (int idx = 1; idx < 8; idx++) {
                int tmp = Cnt[idx];
                Cnt[idx] = lastVal + Cnt[idx-1];
                lastVal = tmp;
            }
        }

        // cal Offset
        if (threadIdx.x < 8){
            int ofst = 0;
            for (int n = 0; n < blockDim.x; n++){
                if (((Morton_In[n] >> (i*3)) & 7) == threadIdx.x)
                    Offset[n] = ofst++;
            } 
        }
        __syncthreads();

        int val = (Morton_In[threadIdx.x] >> i*3) & 7;
        int index = Cnt[val] + Offset[threadIdx.x];
        Morton_Out[index] = Morton_In[threadIdx.x];
        Permutation_Out[index] = Permutation_In[threadIdx.x];

        // swap
        if (i != 9){ // dont swap at last iteration
            swap(Morton_In, Morton_Out);
            swap(Permutation_In, Permutation_Out);
        }
    }

}

__device__ double gravity_device_mass_dev(double m0, double t){
    return m0 + 0.5 * m0 * fabs(sin(t / 6000));
}

union SMEM_TYPE1 {
    unsigned int Morton[2 * MAX_PARTICLES];
    double qmin[MAX_PARTICLES];
};
union SMEM_TYPE2 {
    unsigned int Permutation[2 * MAX_PARTICLES];
    double qmax[MAX_PARTICLES];
};

__global__ void copy_data_to_bhTree(int step, char ignoreDevice){
    // assert <<<1, N>>>
    __shared__ SMEM_TYPE1 smem1; // 8KB
    __shared__ SMEM_TYPE2 smem2; // 8KB
    __shared__ double sQx[MAX_PARTICLES]; // 8KB
    __shared__ double sQy[MAX_PARTICLES]; // 8KB
    __shared__ double sQz[MAX_PARTICLES]; // 8KB
    __shared__ double xmin, xmax, ymin, ymax, zmin, zmax; // 48B
    __shared__ int Cnt[8]; // 32B
    __shared__ int16_t Offset[MAX_PARTICLES]; // 2KB
    __shared__ int numInternalNodes;

    sQx[threadIdx.x] = Input_qxV_cnst[threadIdx.x];
    sQy[threadIdx.x] = Input_qyV_cnst[threadIdx.x];
    sQz[threadIdx.x] = Input_qzV_cnst[threadIdx.x];
    __syncthreads();

    // cal (x, y, z) (min, max)
    bhTree_find_minAndmax(sQx, xmin, xmax, smem1.qmin, smem2.qmax);
    bhTree_find_minAndmax(sQy, ymin, ymax, smem1.qmin, smem2.qmax);
    bhTree_find_minAndmax(sQz, zmin, zmax, smem1.qmin, smem2.qmax);
    __syncthreads();

    // cal morton 
    smem1.Morton[threadIdx.x] = morton3D(sQx[threadIdx.x], sQy[threadIdx.x], sQz[threadIdx.x], xmin, xmax, ymin, ymax, zmin, zmax);

    // init permutation
    smem2.Permutation[threadIdx.x] = threadIdx.x;
    __syncthreads();


    // radix sort 
    radix_sort_Morton(smem1.Morton, smem2.Permutation, Cnt, Offset);

    // copy data to bhTree
    if (threadIdx.x == 0) numInternalNodes = *N_padding_cnst - 1;
    __syncthreads(); 

    bhTree_bxminV_cnst[numInternalNodes + threadIdx.x] = sQx[smem2.Permutation[threadIdx.x]]; // blockDim.x == N, reserve N-1 for internal nodes
    bhTree_bxmaxV_cnst[numInternalNodes + threadIdx.x] = sQx[smem2.Permutation[threadIdx.x]]; 
    bhTree_cxV_cnst[numInternalNodes + threadIdx.x] = sQx[smem2.Permutation[threadIdx.x]]; 
    bhTree_byminV_cnst[numInternalNodes + threadIdx.x] = sQy[smem2.Permutation[threadIdx.x]]; 
    bhTree_bymaxV_cnst[numInternalNodes + threadIdx.x] = sQy[smem2.Permutation[threadIdx.x]]; 
    bhTree_cyV_cnst[numInternalNodes + threadIdx.x] = sQy[smem2.Permutation[threadIdx.x]]; 
    bhTree_bzminV_cnst[numInternalNodes + threadIdx.x] = sQz[smem2.Permutation[threadIdx.x]]; 
    bhTree_bzmaxV_cnst[numInternalNodes + threadIdx.x] = sQz[smem2.Permutation[threadIdx.x]]; 
    bhTree_czV_cnst[numInternalNodes + threadIdx.x] = sQz[smem2.Permutation[threadIdx.x]]; 
    bhTree_widthV_cnst[numInternalNodes + threadIdx.x] = 0;


    double m = Input_mV_cnst[smem2.Permutation[threadIdx.x]];
    if (Input_typeV_cnst[smem2.Permutation[threadIdx.x]] == 'd'){
        if (ignoreDevice) 
            bhTree_mV_cnst[numInternalNodes + threadIdx.x] = 0;
        else
            bhTree_mV_cnst[numInternalNodes + threadIdx.x] = gravity_device_mass_dev(m, step * DT);
    }
    else{
        bhTree_mV_cnst[numInternalNodes + threadIdx.x] = m;
    }

}

__global__ void build_tree(){
    // assert <<<2,N_padding/2>>>
    __shared__ double sBxminV[MAX_PARTICLES/2]; // 4KB
    __shared__ double sByminV[MAX_PARTICLES/2]; // 4KB
    __shared__ double sBzminV[MAX_PARTICLES/2]; // 4KB
    __shared__ double sBxmaxV[MAX_PARTICLES/2]; // 4KB
    __shared__ double sBymaxV[MAX_PARTICLES/2]; // 4KB
    __shared__ double sBzmaxV[MAX_PARTICLES/2]; // 4KB
    __shared__ double sCxV[MAX_PARTICLES/2];    // 4KB
    __shared__ double sCyV[MAX_PARTICLES/2];    // 4KB
    __shared__ double sCzV[MAX_PARTICLES/2];    // 4KB
    __shared__ double sMV[MAX_PARTICLES/2];     // 4KB
    __shared__ double sWidthV[MAX_PARTICLES/2]; // 4KB
    __shared__ int numInternalNodes; 

    // __syncthreads();
    // if (blockIdx.x == 0 && threadIdx.x == 0){
    //     for (int n = 0; n < 2* *N_padding_cnst; n++){
    //         printf("n=%d\n", n);
    //         printf("(%.4e %.4e %.4e) (%.4e %.4e %.4e) (%.4e %.4e %.4e) %.4e %.4e\n\n", bhTree_bxminV_cnst[n], bhTree_byminV_cnst[n], bhTree_bzminV_cnst[n],
    //                                                            bhTree_bxmaxV_cnst[n], bhTree_bymaxV_cnst[n], bhTree_bzmaxV_cnst[n],
    //                                                            bhTree_cxV_cnst[n], bhTree_cyV_cnst[n], bhTree_czV_cnst[n], bhTree_mV_cnst[n], bhTree_widthV_cnst[n]);
    //     }
    // }

    if (threadIdx.x == 0) numInternalNodes = *N_padding_cnst - 1;
    __syncthreads();

    int global_tid = blockIdx.x * blockDim.x + threadIdx.x;
    sBxminV[threadIdx.x] = sBxmaxV[threadIdx.x] = sCxV[threadIdx.x] = bhTree_cxV_cnst[numInternalNodes + global_tid];
    sByminV[threadIdx.x] = sBymaxV[threadIdx.x] = sCyV[threadIdx.x] = bhTree_cyV_cnst[numInternalNodes + global_tid];
    sBzminV[threadIdx.x] = sBzmaxV[threadIdx.x] = sCzV[threadIdx.x] = bhTree_czV_cnst[numInternalNodes + global_tid];
    sMV[threadIdx.x] = bhTree_mV_cnst[numInternalNodes + global_tid];
    sWidthV[threadIdx.x] = 0;
    __syncthreads();

    for (int m = 2, k = (blockDim.x>>1); k >= 1; m=m<<1, k=k>>1){
        if (threadIdx.x < k){
            int parentID = threadIdx.x * m;
            int lChildID = threadIdx.x * m;
            int rChildID = threadIdx.x * m + (m>>1);
            sBxminV[parentID] = MIN2(sBxminV[lChildID], sBxminV[rChildID]);
            sByminV[parentID] = MIN2(sByminV[lChildID], sByminV[rChildID]);
            sBzminV[parentID] = MIN2(sBzminV[lChildID], sBzminV[rChildID]);
            sBxmaxV[parentID] = MAX2(sBxmaxV[lChildID], sBxmaxV[rChildID]);
            sBymaxV[parentID] = MAX2(sBymaxV[lChildID], sBymaxV[rChildID]);
            sBzmaxV[parentID] = MAX2(sBzmaxV[lChildID], sBzmaxV[rChildID]);
            sWidthV[parentID] = MAX3(sBxmaxV[parentID]-sBxminV[parentID], sBymaxV[parentID]-sByminV[parentID], sBzmaxV[parentID]-sBzminV[parentID]);
            double parentm = sMV[lChildID] + sMV[rChildID];
            if (parentm > 0){
                sCxV[parentID] = (sCxV[lChildID] * sMV[lChildID] + sCxV[rChildID] * sMV[rChildID]) / parentm;
                sCyV[parentID] = (sCyV[lChildID] * sMV[lChildID] + sCyV[rChildID] * sMV[rChildID]) / parentm;
                sCzV[parentID] = (sCzV[lChildID] * sMV[lChildID] + sCzV[rChildID] * sMV[rChildID]) / parentm;
            }
            sMV[parentID] = parentm;

            int treeIdx = (k<<1) - 1 + blockIdx.x * k + threadIdx.x ;
            bhTree_bxminV_cnst[treeIdx] = sBxminV[parentID];
            bhTree_byminV_cnst[treeIdx] = sByminV[parentID];
            bhTree_bzminV_cnst[treeIdx] = sBzminV[parentID];
            bhTree_bxmaxV_cnst[treeIdx] = sBxmaxV[parentID];
            bhTree_bymaxV_cnst[treeIdx] = sBymaxV[parentID];
            bhTree_bzmaxV_cnst[treeIdx] = sBzmaxV[parentID];
            bhTree_cxV_cnst[treeIdx] = sCxV[parentID];
            bhTree_cyV_cnst[treeIdx] = sCyV[parentID];
            bhTree_czV_cnst[treeIdx] = sCzV[parentID];
            bhTree_widthV_cnst[treeIdx] = sWidthV[parentID];
            bhTree_mV_cnst[treeIdx] = sMV[parentID];
        } 
        __syncthreads();
    }


    // cal root data
    __shared__ double sRootBmin[3];
    __shared__ double sRootBmax[3];
    __shared__ double sRootm;
    if (blockIdx.x == 0){
        double* treePtrs[] = {bhTree_bxminV_cnst, bhTree_byminV_cnst, bhTree_bzminV_cnst, bhTree_bxmaxV_cnst, bhTree_bymaxV_cnst, bhTree_bzmaxV_cnst,
                              bhTree_cxV_cnst, bhTree_cyV_cnst, bhTree_czV_cnst};
        double* smemPtrs[] = {sBxminV, sByminV, sBzminV, sBxmaxV, sBymaxV, sBzmaxV, sCxV, sCyV, sCzV};

        if (threadIdx.x == 0){
            sRootm = bhTree_mV_cnst[0] = sMV[0] + bhTree_mV_cnst[2]; // update m
        }
        __syncthreads();

        if(threadIdx.x < 3){ 
            sRootBmin[threadIdx.x] = treePtrs[threadIdx.x][0] = MIN2(smemPtrs[threadIdx.x][0], treePtrs[threadIdx.x][2]); // update xyz min
            sRootBmax[threadIdx.x] = treePtrs[threadIdx.x+3][0] = MAX2(smemPtrs[threadIdx.x+3][0], treePtrs[threadIdx.x+3][2]); // update xyz max
            treePtrs[threadIdx.x+6][0] = (smemPtrs[threadIdx.x+6][0] * sMV[0] + treePtrs[threadIdx.x+6][2] * bhTree_mV_cnst[2]) / sRootm;// update cx, cy, cz
        }   
        __syncthreads();

        if (threadIdx.x == 0){
            bhTree_widthV_cnst[0] = MAX3(sRootBmax[0]-sRootBmin[0], sRootBmax[1]-sRootBmin[1], sRootBmax[2]-sRootBmin[2]); // update width 
        }
    }

    // __syncthreads();
    // if (blockIdx.x == 0 && threadIdx.x == 0){
    //     for (int n = 0; n < 2* *N_padding_cnst; n++){
    //         printf("n=%d\n", n);
    //         printf("(%.2e %.2e %.2e) (%.2e %.2e %.2e) (%.2e %.2e %.2e) %.2e %.2e\n\n", bhTree_bxminV_cnst[n], bhTree_byminV_cnst[n], bhTree_bzminV_cnst[n],
    //                                                            bhTree_bxmaxV_cnst[n], bhTree_bymaxV_cnst[n], bhTree_bzmaxV_cnst[n],
    //                                                            bhTree_cxV_cnst[n], bhTree_cyV_cnst[n], bhTree_czV_cnst[n], bhTree_mV_cnst[n], bhTree_widthV_cnst[n]);
    //     }
    // }
}


__device__ void bhTree_cal_accelerations(double* ax, double* ay, double* az, double qx, double qy, double qz, int* workCnt){
    
    int num_worker = MIN2(NUMWORKER, *N_padding_cnst);
    if ((threadIdx.x & (NUMWORKER-1)) < num_worker){

        int16_t recursionStack[RECURSION_STACK_SIZE];
        int16_t* stackPtr = recursionStack;
        *stackPtr++ = -1; // push

        if ((threadIdx.x & (num_worker-1)) == 0){
            *ax = *ay = *az = 0.0;
            *workCnt = 0;
        }
        __syncthreads();

        // printf("\n\nprocessing qx=%.4e qy=%.4e qz=%.4e particle...\n", qx, qy, qz);
        int nodeId = (num_worker-1) + (threadIdx.x & (num_worker-1));
        while (nodeId != -1) { // !stack.empty()
            atomicAdd(workCnt, 1);
            // printf("\t\tnodeId=[%d]\n", nodeId);
            double dx = bhTree_cxV_cnst[nodeId] - qx;
            double dy = bhTree_cyV_cnst[nodeId] - qy;
            double dz = bhTree_czV_cnst[nodeId] - qz;
            double d = sqrt(dx*dx + dy*dy + dz*dz);
            // printf("node[%d] x=%.4e y=%.4e z=%.4e\n", nodeId, bhTree_cxV_cnst[nodeId], bhTree_cyV_cnst[nodeId], bhTree_czV_cnst[nodeId]);
            // printf("node[%d] width=%.4e d=%.4e\n", nodeId, bhTree_widthV_cnst[nodeId], d);

            if (bhTree_widthV_cnst[nodeId] / d > THETA){
                // printf("\t\tnot far enough... look into children...\n");
                int16_t lChildID = (nodeId+1)*2-1;
                int16_t rChildID = (nodeId+1)*2;
                if (bhTree_mV_cnst[lChildID] > 0){
                    *stackPtr++ = lChildID; // push
                }
                if (bhTree_mV_cnst[rChildID] > 0){
                    *stackPtr++ = rChildID; // push
                }
            }
            else {
                // printf("\t\tviewed as a single particle...\n");
                double dist3 = pow(dx*dx + dy*dy + dz*dz + EPS*EPS, 1.5);
                // ax += bhTree_mV_cnst[nodeId] * CONSTANT_G * dx / dist3; 
                // ay += bhTree_mV_cnst[nodeId] * CONSTANT_G * dy / dist3;
                // az += bhTree_mV_cnst[nodeId] * CONSTANT_G * dz / dist3;
                atomicAdd(ax, bhTree_mV_cnst[nodeId] * CONSTANT_G * dx / dist3);
                atomicAdd(ay, bhTree_mV_cnst[nodeId] * CONSTANT_G * dy / dist3);
                atomicAdd(az, bhTree_mV_cnst[nodeId] * CONSTANT_G * dz / dist3);

                // printf("\t\tax=%lf ay=%lf az=%lf\n", ax, ay, az);
            }
            nodeId = *--stackPtr; // pop
        } 
    }

}


__global__ void advance_one_step(){
    int index = (blockDim.x * blockIdx.x + threadIdx.x) / NUMWORKER;
    int stride = (blockDim.x * gridDim.x) / NUMWORKER;

    // extern __shared__ int16_t RecursionStack[]; 
    __shared__ double axV[2048/NUMWORKER]; 
    __shared__ double ayV[2048/NUMWORKER]; 
    __shared__ double azV[2048/NUMWORKER]; 
    __shared__ int Counter[2048/NUMWORKER];

    for (int i = index; i < *N_cnst; i += stride){
        // compute accelerations
        // printf("particle[%d] ax=%lf, ay=%lf, az=%lf\n", i, axV[threadIdx.x>>3], ayV[threadIdx.x>>3], azV[threadIdx.x>>3]);
        int smemIdx = threadIdx.x >> NUMWORKER_POW;
        bhTree_cal_accelerations(&axV[smemIdx], &ayV[smemIdx], &azV[smemIdx], Input_qxV_cnst[i], Input_qyV_cnst[i], Input_qzV_cnst[i], &Counter[smemIdx]);
        __syncthreads();

        if ((threadIdx.x & (NUMWORKER-1)) == 0){

            // printf("particle[%d] ax=%lf, ay=%lf, az=%lf\n", i, axV[threadIdx.x>>3], ayV[threadIdx.x>>3], azV[threadIdx.x>>3]);
            // printf("particle[%d] workload = %d\n", i, Counter[threadIdx.x >> NUMWORKER_POW]);

            // update velocities
            Input_vxV_cnst[i] += axV[smemIdx] * DT;
            Input_vyV_cnst[i] += ayV[smemIdx] * DT;
            Input_vzV_cnst[i] += azV[smemIdx] * DT;

            // update positions
            Input_qxV_cnst[i] += Input_vxV_cnst[i] * DT;
            Input_qyV_cnst[i] += Input_vyV_cnst[i] * DT;
            Input_qzV_cnst[i] += Input_vzV_cnst[i] * DT;
        }
        __syncthreads();

        // printf("particle[%d] ax=%lf, ay=%lf, az=%lf\n", i, ax, ay, az);
        // printf("particle[%d] vx=%lf, vy=%lf, vz=%lf\n", i, Input_vxV_cnst[i], Input_vyV_cnst[i], Input_vzV_cnst[i]);
        // printf("particle[%d] qx=%lf, qy=%lf, qz=%lf\n", i, Input_qxV_cnst[i], Input_qyV_cnst[i], Input_qzV_cnst[i]);
    }
}

__global__ void print_InputV(){
    // assert <<<1,1>>>
    printf("\n InputV:\n");
    for (int i = 0; i < *N_cnst; i++){
        printf("[%d] qx=%lf qy=%lf qz=%lf vx=%lf vy=%lf vz=%lf m=%lf type=%c\n", i, Input_qxV_cnst[i], Input_qyV_cnst[i], Input_qzV_cnst[i], Input_vxV_cnst[i], Input_vyV_cnst[i], Input_vzV_cnst[i], Input_mV_cnst[i], Input_typeV_cnst[i]);
    }
    printf("\n");
}

__global__ void update_min_dist(double* min_dist_dev){
    // assert <<<1,1>>>

    double dx = Input_qxV_cnst[*planetId_cnst] - Input_qxV_cnst[*asteroidId_cnst];
    double dy = Input_qyV_cnst[*planetId_cnst] - Input_qyV_cnst[*asteroidId_cnst];
    double dz = Input_qzV_cnst[*planetId_cnst] - Input_qzV_cnst[*asteroidId_cnst];

    *min_dist_dev = MIN2(*min_dist_dev, sqrt(dx*dx + dy*dy + dz*dz));
    // printf("current min_dist_dev: %lf\n", *min_dist_dev);
}

__global__ void check_planet_asteroid_collision(int* hit_time_step_dev, int step){
    // assert <<<1,1>>>

    double dx = Input_qxV_cnst[*planetId_cnst] - Input_qxV_cnst[*asteroidId_cnst];
    double dy = Input_qyV_cnst[*planetId_cnst] - Input_qyV_cnst[*asteroidId_cnst];
    double dz = Input_qzV_cnst[*planetId_cnst] - Input_qzV_cnst[*asteroidId_cnst];

    if (dx*dx + dy*dy + dz*dz < PLANET_RADIUS*PLANET_RADIUS){
        *hit_time_step_dev = step;
    }
}

__global__ void check_missile_device_collision(int currentStep){
    // assert <<<1, N>>>

    if (Input_typeV_cnst[threadIdx.x] == 'd'){
        
        double dx = Input_qxV_cnst[*planetId_cnst] - Input_qxV_cnst[threadIdx.x];
        double dy = Input_qyV_cnst[*planetId_cnst] - Input_qyV_cnst[threadIdx.x];
        double dz = Input_qzV_cnst[*planetId_cnst] - Input_qzV_cnst[threadIdx.x];
        double missile_travel_dist = currentStep * DT * MISSLE_SPEED;
        if (missile_travel_dist*missile_travel_dist > dx*dx + dy*dy + dz*dz){ // collision
            
            // check if the device has been considered
            char duplicated = 0;
            for (int i = 0; i < *numPendingCase_cnst; i++){
                if (pendingCaseArgV_cnst[i].deviceID == threadIdx.x){
                    duplicated = 1;
                    break;
                }
            }

            if (!duplicated){
                printf("\t\tfind a missile-device[%d] collision at step[%d]\n", threadIdx.x, currentStep);

                int idx = atomicAdd(numPendingCase_cnst, 1); 
                PendingCaseArg arg = (PendingCaseArg){.deviceID = threadIdx.x, .collsionStep = currentStep};
                pendingCaseArgV_cnst[idx] = arg;

                memcpy(Pending_qxVV_cnst[idx], Input_qxV_cnst, (*N_cnst) * sizeof(double));
                memcpy(Pending_qyVV_cnst[idx], Input_qyV_cnst, (*N_cnst) * sizeof(double));
                memcpy(Pending_qzVV_cnst[idx], Input_qzV_cnst, (*N_cnst) * sizeof(double));
                memcpy(Pending_vxVV_cnst[idx], Input_vxV_cnst, (*N_cnst) * sizeof(double));
                memcpy(Pending_vyVV_cnst[idx], Input_vyV_cnst, (*N_cnst) * sizeof(double));
                memcpy(Pending_vzVV_cnst[idx], Input_vzV_cnst, (*N_cnst) * sizeof(double));
                memcpy(Pending_typeVV_cnst[idx], Input_typeV_cnst, (*N_cnst) * sizeof(char));

                double original_m = Input_mV_cnst[threadIdx.x];
                Input_mV_cnst[threadIdx.x] = 0;
                memcpy(Pending_mVV_cnst[idx], Input_mV_cnst, (*N_cnst) * sizeof(double));
                Input_mV_cnst[threadIdx.x] = original_m;

            } // if !duplicated
        } // if collision
    } // if type == d 

}

typedef struct _ProcessPendingCaseArg {
    int caseId;
    int devth;
    int* gravity_device_id;
    double* missile_cost;
} ProcessPendingCaseArg;

// void process_pending_case(int caseId, int devth, int *gravity_device_id, double *missile_cost){
void* process_pending_case(void* argInput){

    int caseId = ((ProcessPendingCaseArg*)argInput)->caseId;
    int devth = ((ProcessPendingCaseArg*)argInput)->devth;
    int* gravity_device_id = ((ProcessPendingCaseArg*)argInput)->gravity_device_id;
    double* missile_cost = ((ProcessPendingCaseArg*)argInput)->missile_cost;

    cudaSetDevice(devth);

    PendingCaseArg arg = pendingCaseArgV_h[caseId];
    cudaMemcpy(Input_qxV_tmp[devth], Pending_qxVV_tmp[caseId], N_h*sizeof(double), cudaMemcpyDeviceToDevice);
    cudaMemcpy(Input_qyV_tmp[devth], Pending_qyVV_tmp[caseId], N_h*sizeof(double), cudaMemcpyDeviceToDevice);
    cudaMemcpy(Input_qzV_tmp[devth], Pending_qzVV_tmp[caseId], N_h*sizeof(double), cudaMemcpyDeviceToDevice);
    cudaMemcpy(Input_vxV_tmp[devth], Pending_vxVV_tmp[caseId], N_h*sizeof(double), cudaMemcpyDeviceToDevice);
    cudaMemcpy(Input_vyV_tmp[devth], Pending_vyVV_tmp[caseId], N_h*sizeof(double), cudaMemcpyDeviceToDevice);
    cudaMemcpy(Input_vzV_tmp[devth], Pending_vzVV_tmp[caseId], N_h*sizeof(double), cudaMemcpyDeviceToDevice);
    cudaMemcpy(Input_mV_tmp[devth], Pending_mVV_tmp[caseId], N_h*sizeof(double), cudaMemcpyDeviceToDevice);
    cudaMemcpy(Input_typeV_tmp[devth], Pending_typeVV_tmp[caseId], N_h*sizeof(char), cudaMemcpyDeviceToDevice);
    
    // printf("process caseId[%d] of deviceId[%d] from step[%d]...\n", caseId, arg.deviceID, arg.collsionStep+1);

    int hit_time_step_h = -2;
    int* hit_time_step_dev;
    cudaMalloc(&hit_time_step_dev, sizeof(int));
    cudaMemcpy(hit_time_step_dev, &hit_time_step_h, sizeof(int), cudaMemcpyHostToDevice);
    for (int step = arg.collsionStep+1; step <= N_STEPS; step++){
        if (step % 10000 == 0) printf("step=%d\n", step);
            
        // copy data to bhTree
        char ignoreDevice = 0;
        copy_data_to_bhTree<<<1, N_h>>>(step, ignoreDevice);

        // construct perfect bhTree
        build_tree<<<2, N_padding_h/2>>>();

        // advance
        advance_one_step<<<NUMBLOCKS, THREADS_PER_BLOCK>>>();
        check_planet_asteroid_collision<<<1,1>>>(hit_time_step_dev, step);
    }

    cudaMemcpy(&hit_time_step_h, hit_time_step_dev, sizeof(int), cudaMemcpyDeviceToHost);
    
    // printf("pendingCase[%d] hit_time_step_h = %d\n", caseId, hit_time_step_h);
    if (hit_time_step_h == -2){ // we prevent the collision between planet & asteroid
        double cost =  param::get_missile_cost((arg.collsionStep+1)*DT);
        *gravity_device_id = arg.deviceID;
        *missile_cost = cost; 
        // printf("no collision!!! gravity_device_id=%d missile_cost=%lf\n", gravity_device_id, missile_cost);
    }

    return 0;
}

void* problem1_solver(void* arg){
    double *min_dist_h = (double*)arg;

    cudaSetDevice(0);
    double *min_dist_dev;
    cudaMalloc(&min_dist_dev, sizeof(double));
    cudaMemcpy(min_dist_dev, min_dist_h, sizeof(double), cudaMemcpyHostToDevice);
    for (int step = 1; step <= N_STEPS; step++){
    // for (int step = 1; step <= 1; step++){
        if (step % 10000 == 0) printf("P1_solver: step=%d\n", step);
            
        // copy data to bhTree
        char ignoreDevice = 1;
        copy_data_to_bhTree<<<1, N_h>>>(step, ignoreDevice);

        // construct perfect bhTree
        build_tree<<<2, N_padding_h/2>>>();

        // advance
        advance_one_step<<<NUMBLOCKS, THREADS_PER_BLOCK>>>();
        update_min_dist<<<1,1>>>(min_dist_dev);
    }
    cudaMemcpy(min_dist_h, min_dist_dev, sizeof(double), cudaMemcpyDeviceToHost); // cudaDeviceSynchronize()
    printf("P1_solver: %.16e\n", *min_dist_h);

    cudaFree(min_dist_dev);
    return 0;
}

int main(int argc, char** argv) {

    // input data
    if (argc != 3) {
        throw std::runtime_error("must supply 2 arguments");
    }
    std::vector<double> qxV_h, qyV_h, qzV_h, vxV_h, vyV_h, vzV_h, mV_h;
    std::vector<char> typeV_h;
    read_input(argv[1], N_h, planetId_h, asteroidId_h, qxV_h, qyV_h, qzV_h, vxV_h, vyV_h, vzV_h, mV_h, typeV_h);


    // copy data from CPU to GPU's Input* arr
    for (int devth=0; devth < NUM_GPU; devth++){ // malloc & copy data & copy arr ptr 
        cudaSetDevice(devth);

        cudaMalloc(&N_tmp[devth], sizeof(int));
        cudaMalloc(&planetId_tmp[devth], sizeof(int));
        cudaMalloc(&asteroidId_tmp[devth], sizeof(int));
        cudaMalloc(&Input_qxV_tmp[devth], N_h*sizeof(double));
        cudaMalloc(&Input_qyV_tmp[devth], N_h*sizeof(double));
        cudaMalloc(&Input_qzV_tmp[devth], N_h*sizeof(double));
        cudaMalloc(&Input_vxV_tmp[devth], N_h*sizeof(double));
        cudaMalloc(&Input_vyV_tmp[devth], N_h*sizeof(double));
        cudaMalloc(&Input_vzV_tmp[devth], N_h*sizeof(double));
        cudaMalloc(&Input_mV_tmp[devth], N_h*sizeof(double));
        cudaMalloc(&Input_typeV_tmp[devth], N_h*sizeof(char));

        cudaMemcpy(N_tmp[devth], &N_h, sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(planetId_tmp[devth], &planetId_h, sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(asteroidId_tmp[devth], &asteroidId_h, sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(Input_qxV_tmp[devth], &qxV_h[0], N_h*sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(Input_qyV_tmp[devth], &qyV_h[0], N_h*sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(Input_qzV_tmp[devth], &qzV_h[0], N_h*sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(Input_vxV_tmp[devth], &vxV_h[0], N_h*sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(Input_vyV_tmp[devth], &vyV_h[0], N_h*sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(Input_vzV_tmp[devth], &vzV_h[0], N_h*sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(Input_mV_tmp[devth], &mV_h[0], N_h*sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(Input_typeV_tmp[devth], &typeV_h[0], N_h*sizeof(char), cudaMemcpyHostToDevice);

        cudaMemcpyToSymbol(N_cnst, &N_tmp[devth], sizeof(int*));
        cudaMemcpyToSymbol(planetId_cnst, &planetId_tmp[devth], sizeof(int*));
        cudaMemcpyToSymbol(asteroidId_cnst, &asteroidId_tmp[devth], sizeof(int*));
        cudaMemcpyToSymbol(Input_qxV_cnst, &Input_qxV_tmp[devth], sizeof(double*));
        cudaMemcpyToSymbol(Input_qyV_cnst, &Input_qyV_tmp[devth], sizeof(double*));
        cudaMemcpyToSymbol(Input_qzV_cnst, &Input_qzV_tmp[devth], sizeof(double*));
        cudaMemcpyToSymbol(Input_vxV_cnst, &Input_vxV_tmp[devth], sizeof(double*));
        cudaMemcpyToSymbol(Input_vyV_cnst, &Input_vyV_tmp[devth], sizeof(double*));
        cudaMemcpyToSymbol(Input_vzV_cnst, &Input_vzV_tmp[devth], sizeof(double*));
        cudaMemcpyToSymbol(Input_mV_cnst, &Input_mV_tmp[devth], sizeof(double*));
        cudaMemcpyToSymbol(Input_typeV_cnst, &Input_typeV_tmp[devth], sizeof(char*));
    }

    // bhTree
    N_padding_h = get_N_padding(N_h);
    for (int devth = 0; devth < NUM_GPU; devth++){ // malloc & copy arr ptr
        cudaSetDevice(devth);

        cudaMalloc(&N_padding_tmp[devth], sizeof(int));
        cudaMalloc(&bhTree_bxminV_tmp[devth], 2*N_padding_h*sizeof(double));
        cudaMalloc(&bhTree_byminV_tmp[devth], 2*N_padding_h*sizeof(double));
        cudaMalloc(&bhTree_bzminV_tmp[devth], 2*N_padding_h*sizeof(double));
        cudaMalloc(&bhTree_bxmaxV_tmp[devth], 2*N_padding_h*sizeof(double));
        cudaMalloc(&bhTree_bymaxV_tmp[devth], 2*N_padding_h*sizeof(double));
        cudaMalloc(&bhTree_bzmaxV_tmp[devth], 2*N_padding_h*sizeof(double));
        cudaMalloc(&bhTree_cxV_tmp[devth], 2*N_padding_h*sizeof(double));
        cudaMalloc(&bhTree_cyV_tmp[devth], 2*N_padding_h*sizeof(double));
        cudaMalloc(&bhTree_czV_tmp[devth], 2*N_padding_h*sizeof(double));
        cudaMalloc(&bhTree_mV_tmp[devth], 2*N_padding_h*sizeof(double));
        cudaMalloc(&bhTree_widthV_tmp[devth], 2*N_padding_h*sizeof(double));

        cudaMemcpy(N_padding_tmp[devth], &N_padding_h, sizeof(int), cudaMemcpyHostToDevice);

        cudaMemcpyToSymbol(N_padding_cnst, &N_padding_tmp[devth], sizeof(int*));
        cudaMemcpyToSymbol(bhTree_bxminV_cnst, &bhTree_bxminV_tmp[devth], sizeof(double*));
        cudaMemcpyToSymbol(bhTree_byminV_cnst, &bhTree_byminV_tmp[devth], sizeof(double*));
        cudaMemcpyToSymbol(bhTree_bzminV_cnst, &bhTree_bzminV_tmp[devth], sizeof(double*));
        cudaMemcpyToSymbol(bhTree_bxmaxV_cnst, &bhTree_bxmaxV_tmp[devth], sizeof(double*));
        cudaMemcpyToSymbol(bhTree_bymaxV_cnst, &bhTree_bymaxV_tmp[devth], sizeof(double*));
        cudaMemcpyToSymbol(bhTree_bzmaxV_cnst, &bhTree_bzmaxV_tmp[devth], sizeof(double*));
        cudaMemcpyToSymbol(bhTree_cxV_cnst, &bhTree_cxV_tmp[devth], sizeof(double*));
        cudaMemcpyToSymbol(bhTree_cyV_cnst, &bhTree_cyV_tmp[devth], sizeof(double*));
        cudaMemcpyToSymbol(bhTree_czV_cnst, &bhTree_czV_tmp[devth], sizeof(double*));
        cudaMemcpyToSymbol(bhTree_mV_cnst, &bhTree_mV_tmp[devth], sizeof(double*));
        cudaMemcpyToSymbol(bhTree_widthV_cnst, &bhTree_widthV_tmp[devth], sizeof(double*));
    }


    // prepare PendingCase memory
    cudaSetDevice(1);

    cudaMalloc(&numPendingCase_tmp, sizeof(int));
    cudaMemcpy(numPendingCase_tmp, &numPendingCase_h, sizeof(int), cudaMemcpyHostToDevice);
    cudaMalloc(&pendingCaseArgV_tmp, MAX_DEVICES*sizeof(PendingCaseArg));
    for (int i = 0; i < MAX_DEVICES; i++){
        cudaMalloc(&Pending_qxVV_tmp[i], N_h*sizeof(double));
        cudaMalloc(&Pending_qyVV_tmp[i], N_h*sizeof(double));
        cudaMalloc(&Pending_qzVV_tmp[i], N_h*sizeof(double));
        cudaMalloc(&Pending_vxVV_tmp[i], N_h*sizeof(double));
        cudaMalloc(&Pending_vyVV_tmp[i], N_h*sizeof(double));
        cudaMalloc(&Pending_vzVV_tmp[i], N_h*sizeof(double));
        cudaMalloc(&Pending_mVV_tmp[i], N_h*sizeof(double));
        cudaMalloc(&Pending_typeVV_tmp[i], N_h*sizeof(char));
    }
    cudaMemcpyToSymbol(numPendingCase_cnst, &numPendingCase_tmp, sizeof(int*));
    cudaMemcpyToSymbol(Pending_qxVV_cnst, &Pending_qxVV_tmp, MAX_DEVICES*sizeof(double*));
    cudaMemcpyToSymbol(Pending_qyVV_cnst, &Pending_qyVV_tmp, MAX_DEVICES*sizeof(double*));
    cudaMemcpyToSymbol(Pending_qzVV_cnst, &Pending_qzVV_tmp, MAX_DEVICES*sizeof(double*));
    cudaMemcpyToSymbol(Pending_vxVV_cnst, &Pending_vxVV_tmp, MAX_DEVICES*sizeof(double*));
    cudaMemcpyToSymbol(Pending_vyVV_cnst, &Pending_vyVV_tmp, MAX_DEVICES*sizeof(double*));
    cudaMemcpyToSymbol(Pending_vzVV_cnst, &Pending_vzVV_tmp, MAX_DEVICES*sizeof(double*));
    cudaMemcpyToSymbol(Pending_mVV_cnst, &Pending_mVV_tmp, MAX_DEVICES*sizeof(double*));
    cudaMemcpyToSymbol(Pending_typeVV_cnst, &Pending_typeVV_tmp, MAX_DEVICES*sizeof(char*));
    cudaMemcpyToSymbol(pendingCaseArgV_cnst, &pendingCaseArgV_tmp, sizeof(PendingCaseArg*));



    // [Problem 1]
    double step0dx = qxV_h[planetId_h] - qxV_h[asteroidId_h];
    double step0dy = qyV_h[planetId_h] - qyV_h[asteroidId_h];
    double step0dz = qzV_h[planetId_h] - qzV_h[asteroidId_h];
    double min_dist_h = sqrt(step0dx*step0dx + step0dy*step0dy + step0dz*step0dz);
    pthread_t problem1_solverTid;
    pthread_create(&problem1_solverTid, NULL, problem1_solver, (void*)&min_dist_h);



    
    // [Problem 2]
    cudaSetDevice(1);
    char doCopy_h = 0;
    char* doCopy_dev;
    cudaMalloc(&doCopy_dev, sizeof(char));
    cudaMemcpy(doCopy_dev, &doCopy_h, sizeof(char), cudaMemcpyHostToDevice);
    int hit_time_step_h = -2;
    int* hit_time_step_dev;
    cudaMalloc(&hit_time_step_dev, sizeof(int));
    step0dx = qxV_h[planetId_h] - qxV_h[asteroidId_h];
    step0dy = qyV_h[planetId_h] - qyV_h[asteroidId_h];
    step0dz = qzV_h[planetId_h] - qzV_h[asteroidId_h];
    if (step0dx*step0dx + step0dy*step0dy + step0dz*step0dz < param::planet_radius * param::planet_radius){
        hit_time_step_h = 0;
    }
    else{
        for (int step = 1; step <= N_STEPS; step++){
        // for (int step = 1; step <= 1; step++){
            if (step % 10000 == 0) printf("step=%d\n", step);
                
            // copy data to bhTree
            char ignoreDevice = 0;
            copy_data_to_bhTree<<<1, N_h>>>(step, ignoreDevice);

            // construct perfect bhTree
            build_tree<<<2, N_padding_h/2>>>();

            // advance
            advance_one_step<<<NUMBLOCKS, THREADS_PER_BLOCK>>>();
            check_planet_asteroid_collision<<<1,1>>>(hit_time_step_dev, step);

            check_missile_device_collision<<<1, N_h>>>(step);
        }
        cudaMemcpy(&hit_time_step_h, hit_time_step_dev, sizeof(int), cudaMemcpyDeviceToHost);
    }
    printf("%d\n", hit_time_step_h);


    pthread_join(problem1_solverTid, NULL);



    // [Problem 3]
    int gravity_device_id = -1;
    double missile_cost = 0;
    if (hit_time_step_h != -2){
        cudaMemcpy(&numPendingCase_h, numPendingCase_tmp, sizeof(int), cudaMemcpyDeviceToHost);
        cudaMemcpy(&pendingCaseArgV_h, pendingCaseArgV_tmp, MAX_DEVICES*sizeof(PendingCaseArg), cudaMemcpyDeviceToHost);

        for (int i = 0; i < numPendingCase_h; i++){
            printf("pending case [%d]: deviceID=%d collisionStep=%d\n", i, pendingCaseArgV_h[i].deviceID, pendingCaseArgV_h[i].collsionStep);
        }

        for (int i = 0; i < numPendingCase_h; i+=2){
            ProcessPendingCaseArg arg0, arg1;

            arg0 = (ProcessPendingCaseArg){.caseId=i, .devth=0, .gravity_device_id = &gravity_device_id, .missile_cost = &missile_cost};
            pthread_t solvePendingTid;
            pthread_create(&solvePendingTid, NULL, process_pending_case, (void*)&arg0);
                
            if (i+1 < numPendingCase_h){
                arg1 = (ProcessPendingCaseArg){.caseId=i+1, .devth=1, .gravity_device_id = &gravity_device_id, .missile_cost = &missile_cost};
                process_pending_case((void*)&arg1);
            }

            pthread_join(solvePendingTid, NULL);

            if (gravity_device_id != -1) break;
        }

    } // if hit_time_step_h != -2



    cudaDeviceSynchronize();
    write_output(argv[2], min_dist_h, hit_time_step_h, gravity_device_id, missile_cost);


    for (int devth = 0; devth < NUM_GPU; devth++){ // Free memory
        cudaSetDevice(devth);

        cudaFree(N_tmp[devth]);
        cudaFree(planetId_tmp[devth]);
        cudaFree(asteroidId_tmp[devth]);
        cudaFree(Input_qxV_tmp[devth]);
        cudaFree(Input_qyV_tmp[devth]);
        cudaFree(Input_qzV_tmp[devth]);
        cudaFree(Input_vxV_tmp[devth]);
        cudaFree(Input_vyV_tmp[devth]);
        cudaFree(Input_vzV_tmp[devth]);
        cudaFree(Input_mV_tmp[devth]);
        cudaFree(Input_typeV_tmp[devth]);

        cudaFree(N_padding_tmp[devth]);
        cudaFree(bhTree_bxminV_tmp[devth]);
        cudaFree(bhTree_byminV_tmp[devth]);
        cudaFree(bhTree_bzminV_tmp[devth]);
        cudaFree(bhTree_bxmaxV_tmp[devth]);
        cudaFree(bhTree_bymaxV_tmp[devth]);
        cudaFree(bhTree_bzmaxV_tmp[devth]);
        cudaFree(bhTree_cxV_tmp[devth]);
        cudaFree(bhTree_cyV_tmp[devth]);
        cudaFree(bhTree_czV_tmp[devth]);
        cudaFree(bhTree_mV_tmp[devth]);
        cudaFree(bhTree_widthV_tmp[devth]);
    }



    return 0;
}

