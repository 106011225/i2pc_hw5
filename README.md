# Parallel computing: N-body simulation with **CUDA**


## N-body Simulation

- N-body is used to predict the motion of celestial bodies. We break continuous time into discrete **time steps**. The time difference of each time step is given by $`\Delta t`$
- At each time step:
    1. Calculate the accelerations applied to each body
    2. Update the velocities using the accelerations of each body
    3. Update the positions using the velocities of each body


## Goal

- Run the N-body simulation for a given amount of time steps and predict the outcome.
- Parallelize the code with CUDA
- [The current implementation](./hw5.cu) applies the concept of [Barnes-Hut Algorithm](https://en.wikipedia.org/wiki/Barnes%E2%80%93Hut_simulation)


## Usage

1. Compile and execute the source code
    ```
    $ make
    $ ./hw5 input_file output_file
    ```

**Please refer to [SPEC.pdf](./SPEC.pdf) and [report.pdf](./report.pdf) for more details.**

