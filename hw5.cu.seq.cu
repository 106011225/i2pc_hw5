#include <cmath>
#include <fstream>
#include <iomanip>
#include <limits>
#include <stdexcept>
#include <string>
#include <vector>
#include <omp.h>

namespace param {
    const int n_steps = 200000;
    const double dt = 60;
    const double eps = 1e-3;
    const double G = 6.674e-11;
    double gravity_device_mass(double m0, double t) {
        return m0 + 0.5 * m0 * fabs(sin(t / 6000));
    }
    const double planet_radius = 1e7;
    const double missile_speed = 1e6;
    double get_missile_cost(double t) { return 1e5 + 1e3 * t; }
}  // namespace param

void read_input(const char* filename, int& N, int& planetId, int& asteroidId,
    std::vector<double>& qx, std::vector<double>& qy, std::vector<double>& qz,
    std::vector<double>& vx, std::vector<double>& vy, std::vector<double>& vz,
    std::vector<double>& m, std::vector<std::string>& type) {

    std::ifstream fin(filename);
    fin >> N >> planetId >> asteroidId;
    qx.resize(N);
    qy.resize(N);
    qz.resize(N);
    vx.resize(N);
    vy.resize(N);
    vz.resize(N);
    m.resize(N);
    type.resize(N);
    for (int i = 0; i < N; i++) {
        fin >> qx[i] >> qy[i] >> qz[i] >> vx[i] >> vy[i] >> vz[i] >> m[i] >> type[i];
    }
}

void write_output(const char* filename, double min_dist, int hit_time_step,
    int gravity_device_id, double missile_cost) {

    std::ofstream fout(filename);
    fout << std::scientific
         << std::setprecision(std::numeric_limits<double>::digits10 + 1) << min_dist
         << '\n'
         << hit_time_step << '\n'
         << gravity_device_id << ' ' << missile_cost << '\n';
}

void run_step(int step, int n, std::vector<double>& qx, std::vector<double>& qy,
    std::vector<double>& qz, std::vector<double>& vx, std::vector<double>& vy,
    std::vector<double>& vz, const std::vector<double>& m,
    const std::vector<std::string>& type) {

    // compute accelerations
    std::vector<double> ax(n), ay(n), az(n);
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            if (j == i) continue;
            double mj = m[j];
            if (type[j] == "device") {
                mj = param::gravity_device_mass(mj, step * param::dt);
            }
            double dx = qx[j] - qx[i];
            double dy = qy[j] - qy[i];
            double dz = qz[j] - qz[i];
            double dist3 =
                pow(dx * dx + dy * dy + dz * dz + param::eps * param::eps, 1.5);
            ax[i] += param::G * mj * dx / dist3;
            ay[i] += param::G * mj * dy / dist3;
            az[i] += param::G * mj * dz / dist3;
        }
    }

    // update velocities
    for (int i = 0; i < n; i++) {
        vx[i] += ax[i] * param::dt;
        vy[i] += ay[i] * param::dt;
        vz[i] += az[i] * param::dt;
    }

    // update positions
    for (int i = 0; i < n; i++) {
        qx[i] += vx[i] * param::dt;
        qy[i] += vy[i] * param::dt;
        qz[i] += vz[i] * param::dt;
    }

    for (int i = 0; i < n; i++){
        // printf("particle[%d] ax=%lf, ay=%lf, az=%lf\n", i, ax[i], ay[i], az[i]);
        // printf("particle[%d] vx=%lf, vy=%lf, vz=%lf\n", i, vx[i], vy[i], vz[i]);
        // printf("particle[%d] qx=%lf, qy=%lf, qz=%lf\n", i, qx[i], qy[i], qz[i]);
    }
}

int main(int argc, char** argv) {
    if (argc != 3) {
        throw std::runtime_error("must supply 2 arguments");
    }
    int N, planetId, asteroidId;
    std::vector<double> qx, qy, qz, vx, vy, vz, m;
    std::vector<std::string> type;

    auto distance = [&](int i, int j) -> double {
        double dx = qx[i] - qx[j];
        double dy = qy[i] - qy[j];
        double dz = qz[i] - qz[j];
        return sqrt(dx * dx + dy * dy + dz * dz);
    };

    // Problem 1
    double min_dist = std::numeric_limits<double>::infinity();
    read_input(argv[1], N, planetId, asteroidId, qx, qy, qz, vx, vy, vz, m, type);
    for (int i = 0; i < N; i++) { // Problem 1 assume that there are no devices
        if (type[i] == "device") {
            m[i] = 0;
        }
    }
    for (int step = 0; step <= param::n_steps; step++) {
    // for (int step = 0; step <= 1; step++) {
        if (step > 0) {
            run_step(step, N, qx, qy, qz, vx, vy, vz, m, type);
        }

        double dx = qx[planetId] - qx[asteroidId];
        double dy = qy[planetId] - qy[asteroidId];
        double dz = qz[planetId] - qz[asteroidId];
        // printf("dx=%lf dy=%lf dz=%lf, d=%lf\n", dx, dy, dz, sqrt(dx * dx + dy * dy + dz * dz));

        min_dist = std::min(min_dist, sqrt(dx * dx + dy * dy + dz * dz));
        // if (step % 10000 == 0) printf("step=%d, current min_dist: %.16e\n", step, min_dist);
    }

    // Problem 2
    int hit_time_step = -2;
    read_input(argv[1], N, planetId, asteroidId, qx, qy, qz, vx, vy, vz, m, type);
    for (int step = 0; step <= param::n_steps; step++) {
        if (step > 0) {
            run_step(step, N, qx, qy, qz, vx, vy, vz, m, type);
        }

        double dx = qx[planetId] - qx[asteroidId];
        double dy = qy[planetId] - qy[asteroidId];
        double dz = qz[planetId] - qz[asteroidId];
        if (dx * dx + dy * dy + dz * dz < param::planet_radius * param::planet_radius) {
            hit_time_step = step;
            break;
        }
    }

    // Problem 3
    // TODO
    int gravity_device_id = -1;
    double missile_cost = 0;

    if (hit_time_step != -2) {
        read_input(argv[1], N, planetId, asteroidId, qx, qy, qz, vx, vy, vz, m, type);
        std::vector<int> deviceIdVec;
        for (int i = 0; i < N; i++){ // collect all deviceId
            if (type[i] == "device"){
                deviceIdVec.push_back(i);
            }
        }
        #pragma omp parallel for private(N, planetId, asteroidId, qx, qy, qz, vx, vy, vz, m, type)
        for (int deviceId : deviceIdVec){
            read_input(argv[1], N, planetId, asteroidId, qx, qy, qz, vx, vy, vz, m, type);
            double tmp_missile_cost = -1;
            for (int step = 0; step <= param::n_steps; step++) {
                if (step > 0) {
                    run_step(step, N, qx, qy, qz, vx, vy, vz, m, type);
                }

                // check if missile hits device
                if (tmp_missile_cost == -1) {
                    double dx = qx[planetId] - qx[deviceId];
                    double dy = qy[planetId] - qy[deviceId];
                    double dz = qz[planetId] - qz[deviceId];
                    double missile_travel_dist = step*param::dt*param::missile_speed;
                    if (missile_travel_dist*missile_travel_dist > dx * dx + dy * dy + dz * dz) {
                        tmp_missile_cost = param::get_missile_cost((step+1)*param::dt); 
                        m[deviceId] = 0;
                    }
                }

                // check if asteroid hits planet
                double dx = qx[planetId] - qx[asteroidId];
                double dy = qy[planetId] - qy[asteroidId];
                double dz = qz[planetId] - qz[asteroidId];
                if (dx * dx + dy * dy + dz * dz < param::planet_radius * param::planet_radius) {
                    tmp_missile_cost = 0;
                    break;
                }
            } // for each step

            // updatae missile cost
            if (tmp_missile_cost != 0) {
                if (missile_cost == 0){
                    gravity_device_id = deviceId;
                    missile_cost = tmp_missile_cost;
                } 
                else if (tmp_missile_cost < missile_cost) {
                    gravity_device_id = deviceId;
                    missile_cost = tmp_missile_cost;
                }
            }

        } // for each deviceId
    } // if the asteroid will hit the planet

    write_output(argv[2], min_dist, hit_time_step, gravity_device_id, missile_cost);
}
